#!/bin/bash

# source .env


USER="XXXXXX"
PASSWORD="YYYYYY"
ORGANISATION="loyaltoid"
REPO_LAMA=$1
REPO_BARU=$2
PROJECT_KEY=$3 #---> KOR /PG / GEN 
DEV="develop" #--> develop / development
STA="staging"


curl -X POST -v -u ${USER}:${PASSWORD} -H "Content-Type: application/json" \
  https://api.bitbucket.org/2.0/repositories/${ORGANISATION}/${REPO_BARU} \
  -d '{"scm": "git", "is_private": "true", "fork_policy": "no_public_forks", "project": {"key": "'${PROJECT_KEY}'"}}'

rm -rf ${REPO_LAMA}
git clone git@bitbucket.org:${ORGANISATION}/${REPO_LAMA}.git
cd ${REPO_LAMA}
git remote set-url origin git@bitbucket.org:${ORGANISATION}/${REPO_BARU}.git
git push origin master
git checkout ${DEV}
git push origin ${DEV}
git checkout ${STA}
git push origin ${STA}
cd ../
rm -rf ${REPO_LAMA}